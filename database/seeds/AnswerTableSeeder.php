<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AnswerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('text'=>'какой-то ответ', 'question_id' =>4, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')),
            array('text'=>'какой-то ответ 1', 'question_id' =>5, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')),
            array('text'=>'какой-то ответ 2', 'question_id' =>6, 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s'))
        );
        DB::table('answers')->insert($data);
    }
}
