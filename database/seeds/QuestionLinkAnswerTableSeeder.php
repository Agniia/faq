<?php

use Illuminate\Database\Seeder;

class QuestionLinkAnswerTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('question_id'=>1, 'answer_id'=>1),
            array('question_id'=>2, 'answer_id'=>2),
            array('question_id'=>3, 'answer_id'=>3)
        );
        DB::table('question_link_answer')->insert($data);
    }
}
