<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;
class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('author_id'=>1, 'category_id'=>1,'status_id'=>1,'question'=>'до коле это будет продолжаться','created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')),
            array('author_id'=>2, 'category_id'=>2,'status_id'=>2,'question'=>'где я', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')),
            array('author_id'=>3, 'category_id'=>3,'status_id'=>3, 'question'=>'кто я', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s'))
        );
        DB::table('questions')->insert($data);
    }
}
