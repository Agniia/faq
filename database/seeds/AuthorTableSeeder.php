<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AuthorTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = array(
            array('name'=>'ivan', 'email' => 'ivan@ivan.ru', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')),
            array('name'=>'peter', 'email' => 'peter@peter.ru', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s')),
            array('name'=>'sidor', 'email' => 'sidor@sidor.ru', 'created_at' => Carbon::now()->format('Y-m-d H:i:s'), 'updated_at' => Carbon::now()->format('Y-m-d H:i:s'))
        );
         DB::table('authors')->insert($data);
    }
}
