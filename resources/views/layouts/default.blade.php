<!doctype html>
<html>
    <head>
        @include('includes.head')
    </head>
    <body class="faq"> 
    <header>
        @include('includes.header')
    </header>
    <main role="main" class="container-fluid">
        <div id="main" class="container-fluid">
    
                @yield('content')
    
        </div><!-- main -->
    </main><!-- container -->
    <footer class="footer bg-dark">
        @include('includes.footer')
    </footer>
</body>
</html>