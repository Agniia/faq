<!doctype html>
<html>
<head>
    @include('includes.head')
</head>
<body class="faq">
    <header>
        @include('includes.header')
    </header>
    <div class="container-fluid">
    <main role="main" class="container-fluid">
        <div class="row">
            <!-- sidebar content -->
            <div id="sidebar" class="col-md-3">
                @include('includes.sidebar')
            </div><!--sidebar-->
    
            <!-- main content -->
            <div id="content" class="col-md-9">
                @yield('content')
            </div><!--content-->
        </div>
    </main>
    </div>
    <footer class="footer bg-dark">
        @include('includes.footer')
    </footer>
</body>
</html>