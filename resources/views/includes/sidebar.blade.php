    <!-- sidebar nav -->
    <nav id="sidebar-nav">
        <ul>
    	  @foreach ($links as $key=>$link)
                 <li>
                    <a href="{{ $link }}">{{ $key }}</a>
                </li>
            @endforeach
        </ul>
    </nav>
