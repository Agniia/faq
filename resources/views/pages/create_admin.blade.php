@section('content')
	    <h3>{{ $title }}</h3>
	    <form action="/admin/store" method="post">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	    <div class="form-group">
		    <label for="login">Логин</label>
		    <input type="text" class="form-control" id="login" name="login" aria-describedby="login" placeholder="Укажите логин" required>	   
	   </div>
	   <div class="form-group">
		    <label for="password">Пароль</label>
		    <input type="text" class="form-control" id="password" name="password" aria-describedby="password" placeholder="Укажите пароль" required>	   
	   </div>
	  <button type="submit" class="btn btn-primary">Добавить администратора</button>
	</form>
@stop
