@section('content')
	    <h3>{{ $title }}</h3>
	    <form action="/faq/question/store">
	    <div class="form-group">
	        <label for="author">Ваше имя</label>
		    <input type="text" class="form-control" id="author" name="author" aria-describedby="author" placeholder="введите Ваше имя" required>	 
	 	</div>  
	 	<div class="form-group">
		    <label for="email">Ваша почта</label>
		    <input type="email" class="form-control" id="email" name="email" aria-describedby="email" placeholder="введите Вашу почту" required>	
		</div>
		<div class="form-group">
		    <label for="cats">Выберете категорию вопроса</label>
		    <select class="form-control" name="cats" id="cats" required>
		    	@foreach($cats as $cat)
			  		<option value="{{ $cat->id }}">{{ $cat->name }}</option>
			  	@endforeach
			</select>
	 </div>
	  <div class="form-group">	
		    <label for="question">Текст вопроса</label>
		    <input type="text" class="form-control" id="question" name="question" aria-describedby="question" placeholder="введите Ваш вопрос" required>	   
	  </div>
	  <button type="submit" class="btn btn-primary">Задать вопрос</button>
	</form>
@stop
