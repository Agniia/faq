@section('content')
	    <h3>{{ $title }}</h3>
	      <table class="table table-striped">
	      	 <thead class="thead-dark">
		      	 <tr>
		      	 	<th>Логин</th>
		      	 	<th>Пароль</th>
		      	 	<th>Дата создания</th>
		      	 	<th>Дата изменения</th>
		      	 </tr>
	      	 </thead>
	      	  <tbody>
		    	   @foreach ($admins as $admin)
		                  <tr> 
		                 	    <td><a href="/admin/show/{{ $admin['id'] }}"> {{ $admin['login'] }}</a></td>	
		                 	    <td> {{  $admin['password'] }}</td>	
		                 	    <td> {{ $admin['created_at'] }}</td>	
		                 	    <td> {{ $admin['updated_at'] }}</td>	
					      </tr>		 	
		            @endforeach
	          </tbody>
        </table>
@stop
