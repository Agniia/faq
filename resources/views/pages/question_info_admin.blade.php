@section('content')
	    <h3>{{ $title }}</h3>
	    @if($qu)
        <div>Вопрос: "{{ $question[0]->question }}"</div>
        <div>Категория: "{{ $question[0]->category_name }}"</div>
        <div>Автор: "{{ $question[0]->author_name }}"</div>
        <div>Ответ: "{{ $question[0]->answer }}"</div>
        <div>Статус: "{{ $question[0]->status_name }}"</div>
        <a href="{{ /admin/question/edit/$question[0]->id }}">Редактировать вопрос</a>        
    	@endif
@stop
