@section('content')
	    <h3>{{ $title }}</h3>
	    <div>Категория № {{ $category_id }}</div>
    	<div>Название категории: "{{ $name }}"</div>
    	<a href="/admin/questions/category/{{ $category_id }}" class="btn btn-primary">Смотреть список вопросов</a>
    	<a href="/admin/category/edit/{{ $category_id }}" class="btn btn-primary">Редактировать категорию</a>
    	<a href="/admin/category/destroy/{{ $category_id }}" class="btn btn-primary">Удалить категорию и все вопросы в ней</a>
@stop
