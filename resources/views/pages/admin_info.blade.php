@section('content')
	    <h3>{{ $title }}</h3>
	      <table class="table table-striped">
	      	 <thead class="thead-dark">
		      	 <tr>
		      	 	<th>Логин</th>
		      	 	<th>Пароль</th>
		      	 	<th>Дата создания</th>
		      	 	<th>Дата изменения</th>
		      	 </tr>
	      	 </thead>
	      	  <tbody>
                  <tr> 
                 	    <td> {{ $admin['login'] }} </td>	
                 	    <td> {{  $admin['password'] }}</td>	
                 	    <td> {{ $admin['created_at'] }}</td>	
                 	    <td> {{ $admin['updated_at'] }}</td>	
			      </tr>		 	
	          </tbody>
        </table>
        <a href="/admin/edit/{{ $admin['id'] }}" class="btn btn-primary">Измененить пароль администратора</a>
        <a href="/admin/destroy/{{ $admin['id'] }}" class="btn btn-primary">Удалить администратора</a>
@stop
