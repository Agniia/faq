@section('content')
	   <h3>{{ $title }}</h3>
	   <form action="/admin/update/{{$admin['id']}}" method="post">
	      <input type="hidden" name="_token" value="{{ csrf_token() }}">
		  <input type="hidden" class="form-control" id="admin" name="admin" value="{{ $admin['id'] }}">	   
	      <div class="form-group">
	     	   <label>Логин администратора {{ $admin['login'] }}</label>
	       </div>
		  <div class="form-group">
			    <label for="password">Новый пароль администратора </label>
		  		<input type="text" class="form-control" id="password" name="password" required>	 
		  </div>
	  <button type="submit" class="btn btn-primary">Сохранить изменения</button>
	 </form>
@stop
