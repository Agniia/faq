@section('content')
		<h3>{{ $title }}</h3>
			<div>Категория № {{ $cat->id }}</div>
	        <div class="m-b-md">Название категории: "{{ $cat->name }}"</div>
	    <h4>{{ $subTitle }}</h4>
	    <ul class="faqs-list">
		 @foreach ($questions as $question)
		  <li>
		  		<h5>Автор вопроса: {{ $question->author_name }}</h5>		  	
		  		<div></div>	
		  		<h5>Дата вопроса: {{ $question->created_at }}</h5>		  	
		  		<div></div>	
		  		<h5>Вопрос:</h5>
		  		<div>{{ $question->question }}</div>
		  		<h5>Ответ:</h5>
		  		<div>{{ $question->answer }}</div>
		  </li>	
         @endforeach
	    </ul>
@stop
