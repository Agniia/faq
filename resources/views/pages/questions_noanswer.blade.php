@section('content')
	    <table class="table table-striped">
	      	 <thead class="thead-dark">
		      	 <tr>
		      	    <th>Номер вопроса</th>
		      	 	<th>Вопрос</th>
		      	 	<th>Добавлен</th>
		      	 	<th>Изменен</th>
		      	 </tr>
	      	 </thead>
	      	  <tbody>
		    	   @foreach ($cont as $data)
		                  <tr> 
		                  		<td><a href="/admin/question/edit/{{$data->id}}">{{ $data->id }}</a></td>	
		                 	    <td> {{ $data->question }}</td>	
		                 	    <td> {{ $data->created_at }}</td>	
		                 	    <td> {{ $data->updated_at }}</td>	
					      </tr>		 	
		            @endforeach
	          </tbody>
        </table>
@stop
