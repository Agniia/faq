@section('content')
<div class="row">
	<div class="col-6">
	    <h3>{{ $title }}</h3>
	    <form action="/admin/login" method="post">
		    <input type="hidden" name="_token" value="{{ csrf_token() }}">
		    <div class="form-group">
			    <label for="login">Логин</label>
			    <input type="text" class="form-control" id="login" name="login" aria-describedby="login" placeholder="Укажите логин" required>	   
		   </div>
		   <div class="form-group">
			    <label for="password">Пароль</label>
			    <input type="text" class="form-control" id="password" name="password" aria-describedby="password" placeholder="Укажите пароль" required>	   
		   </div>
		  <button type="submit" class="btn btn-primary">Войти</button>
		</form>
	</div>
	<div class="col-6">
			<h3><a href="/admin/logout">Выйти</a></h3>
	</div>
</div>
@stop