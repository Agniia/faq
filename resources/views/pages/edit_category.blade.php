@section('content')
	    <h3>{{ $title }}</h3>
	    <form action="/admin/category/update/{{ $cat->id }}">
	    <div class="form-group">
		    <label for="categoryName">Название категории</label>
		    <input type="text" class="form-control" id="categoryName" name="categoryName" aria-describedby="categoryName" value="{{ $cat->name }}" placeholder="{{ $cat->name }}">	   
	  </div>
	  <button type="submit" class="btn btn-primary">Сохранить изменения</button>
	</form>
@stop
