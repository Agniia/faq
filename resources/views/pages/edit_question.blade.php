@section('content')
	    <h3>{{ $title }}</h3>
	    <form action="/admin/question/update/{{$question[0]->id}}" method="post">
	    <input type="hidden" name="_token" value="{{ csrf_token() }}">
	    <div class="form-group">
		    <input type="hidden" class="form-control" id="question" name="question" aria-describedby="categoryName" value="{{ $question[0]->id }}" placeholder="{{ $question[0]->question }}">	   
	   </div>
	  <div class="form-group">
		    <label for="author">Автор</label>
		    <select class="form-control" name="author" id="author">
		    <option value="{{ $question[0]->author_id }}" selected>{{ $question[0]->author_name }}</option>
		    @foreach($authors as $author)
			  <option value="{{ $author->id }}">{{ $author->name }}</option>
			@endforeach
			</select>
	  </div>
	  <div class="form-group">
		    <label for="category">Категория</label>
		    <select class="form-control" name="category" id="category">
		    <option value="{{ $question[0]->category_id }}" selected>{{ $question[0]->category_name }}</option>
		    @foreach($cats as $cat)
			  <option value="{{ $cat->id }}">{{ $cat->name }}</option>
			@endforeach
			</select>
	  </div>
	   @if( $question[0]->answer != NULL) 	   
	        <div class="form-group">
		    	<label for="answer">Редактировать ответ</label>
		    	 <input type="hidden" name="answer_id" value="{{ $question[0]->answer_id }}">
	 		    <input type="text" class="form-control" id="answer" name="answer" aria-describedby="answer" value="{{ $question[0]->answer }}" placeholder="{{ $question[0]->answer }}">	   
		     </div>
		    @else 
		     <div class="form-group">
			     <label for="answer">Добавить ответ</label>
	 		    <input type="text" class="form-control" id="answer" name="answer" aria-describedby="answer" value="{{ $question[0]->answer }}" placeholder="{{ $question[0]->answer }}">	   
		     </div>
	   @endif
	   	<div class="form-group">
		    @if($question[0]->status_id == 2)
		     	<p><input name="status" type="radio" value="2" checked>Опубликовать</p>
		     	<p><input name="status" type="radio" value="3"> Скрыть</p>
		   @else
		   		<p><input name="status" type="radio" value="2">Опубликовать</p>
	    		<p><input name="status" type="radio" value="3" checked> Скрыть</p>
	       @endif
	    </div>	   
	  <button type="submit" class="btn btn-primary">Сохранить изменения</button>
	 </form>
@stop
