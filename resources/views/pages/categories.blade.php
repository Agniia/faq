@section('content')
	    <h3>{{ $title }}</h3>
	      <table class="table table-striped">
	      	 <thead class="thead-dark">
		      	 <tr>
		      	 	<th>№ категории</th>
		      	 	<th>Название категории</th>
		      	 	<th>Всего вопросов</th>
		      	 	<th>Количество вопросов без ответа</th>
		      	 	<th>Количество опубликованных вопросов</th>
		      	 </tr>
	      	 </thead>
	      	  <tbody>
		    	   @foreach ($categories as $category)
		                  <tr> 
		                 	    <td>{{ $category->category_id }}</td>	
		                 	    <td><a href="/admin/category/show/{{ $category->category_id }}"> {{ $category->category_name }}</a></td>	
		                 	    <td> {{ $category->questions_amount }}</td>	
		                 	    <td> {{ $category->no_answer }}</td>	
		                 	    <td> {{ $category->published }}</td>	
					      </tr>		 	
		            @endforeach
	          </tbody>
        </table>
@stop
