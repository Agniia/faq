@section('content')
	    <h3>{{ $title }}</h3>
	    @if($cat)
        <div>Категория № {{ $cat->id }}</div>
        <div>Название категории: "{{ $cat->name }}"</div>
    	<div><a href="/admin/category/edit/{{ $cat->id }}">Редактировать категорию</a></div>
    	@endif
@stop
