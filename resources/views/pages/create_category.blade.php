@section('content')
	    <h3>{{ $title }}</h3>
	    <form action="/admin/category/store">
	    <div class="form-group">
		    <label for="categoryName">Название категории</label>
		    <input type="text" class="form-control" id="categoryName" name="categoryName" aria-describedby="categoryName" placeholder="Укажите название новой категории" required>	   
	  </div>
	  <button type="submit" class="btn btn-primary">Добавить категорию</button>
	</form>
@stop
