@section('content')
	    <h3>{{ $title }}</h3>
	    <table class="table table-striped">
	      	 <thead class="thead-dark">
		      	 <tr>
		      	    <th>Номер вопроса</th>
		      	 	<th>Вопрос</th>
		      	 	<th>Автор</th>
		      	 	<th>Статус</th>
		      	 	<th>Добавлен</th>
		      	 	<th>Изменен</th>
		      	 </tr>
	      	 </thead>
	      	  <tbody>
		    	   @foreach ($cont as $data)
		                  <tr> 
		                  		<td><a href="/admin/question/edit/{{$data->id}}">{{ $data->id }}</a></td>	
		                 	    <td> {{ $data->question }}</td>	
		                 	    <td>{{ $data->author_name }}</td>	
		                 	    <td> {{ $data->status_name }} </td>	
		                 	    <td> {{ $data->created_at }}</td>	
		                 	    <td> {{ $data->updated_at }}</td>	
					      </tr>		 	
		            @endforeach
	          </tbody>
        </table>
@stop
