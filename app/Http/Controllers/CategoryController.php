<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Carbon\Carbon;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
	    $cat = new Category;
	    $categories =  $cat->catLinkQuestionCount();
        $title = 'Категории вопросов';
    	return view('pages.categories')->with('title', $title)->with('categories', $categories); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Добавить категорию';
        return view('pages.create_category')->with('title', $title); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {        
        $cat = new Category;
        $cat->name = $request->categoryName;
        $cat->created_at = Carbon::now()->format('Y-m-d H:i:s');
        $cat->updated_at = Carbon::now()->format('Y-m-d H:i:s'); 
        $title = ($cat->save()) ? 'Категория добавлена' : 'Категория не добавлена';
        return view('pages.category_info')->with('title', $title)->with('cat', $cat); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id,$is_admin)
    {
       $cont = Category::find($id);
       $title = 'Категория вопросов "'.$cont->name.'"';
       if($is_admin)
            return view('pages.category_admin_tpl')->with('title', $title)->with('category_id', $id)->with('name', $cont->name); 
        else
            return view('pages.category_author_tpl')->with('title', $title)->with('category_id', $id)->with('name', $cont->name); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cat = Category::find($id);
        $title = 'Редактировать категорию "'.$cat->name.'"';
        return view('pages.edit_category')->with('title', $title)->with('cat', $cat); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $name = $request->input('categoryName');
        $cat = Category::find($id);
        $cat->name = $name;
        $title = ($cat->save()) ? 'Изменения в категорию внесены' : 'Изменения в категорию не внесены' ;
        return view('pages.category_info')->with('title', $title)->with('cat', $cat); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {                
         $cat = Category::find($id);
         $cat->delete();   
    }
}
