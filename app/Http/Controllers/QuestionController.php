<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Question;
use App\Category;
use App\Author;
use Carbon\Carbon;
        
class QuestionController extends Controller
{
     
     public function showQuestionsByCategory($category_id, $is_admin)
     {
        $cat = Category::find($category_id);
        $title = '"'.$cat->name.'"';
        $subTitle = 'Вопросы категории "'.$cat->name.'"';
        $qu = new Question;
        if($is_admin){
            $questions = $qu->questionsLinkCatTotalInfo($category_id);
            return view('pages.questions_admin_tpl')->with('title', $title)->with('category_id', $category_id)->with('cont', $questions); 
        }
        else{
            $questions = $qu->questionsLinkCatTotalInfoPublished($category_id);
            return view('pages.questions_author_tpl')->with('title', $title)->with('subTitle', $subTitle)->with('cat', $cat)->with('questions', $questions); 
        }
     }
     
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $title = 'Добавить категорию';
        $cats = Category::all();
        return view('pages.create_question')->with('title', $title)->with('cats', $cats); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $author_id)
    {
        $qu = new Question;
        $qu->author_id = $author_id;
        $qu->category_id = $request->cats;
        $qu->status_id = 1;
        $qu->question = $request->question;     
        $qu->created_at = Carbon::now()->format('Y-m-d H:i:s');
        $qu->updated_at = Carbon::now()->format('Y-m-d H:i:s'); 
        $title = ($qu->save()) ? 'Вопрос добавлен' : 'Вопрос не добавлен';
        return view('pages.question_info_author')->with('title', $title)->with('qu', $qu); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $qu = new Question;
        $question = $qu->questionWithAnswerTotalInfo($id);
        $title = 'Bопрос № "'.$question[0]->id.'"';
        return view('pages.question_info_admin')->with('title', $title)->with('question', $question); 
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $qu = new Question;
        $question = $qu->questionWithAnswerTotalInfo($id);
        $authors = Author::all();
        $cats = Category::all();
        $title = 'Редактировать вопрос № "'.$question[0]->id.'"';
        return view('pages.edit_question')->with('title', $title)->with('question', $question)->with('authors',$authors)->with('cats',$cats); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
            $qu = Question::find($id);
            $qu->author_id = $request->author;
            $qu->category_id = $request->category;
            $qu->status_id = $request->status;
            $qu->updated_at = Carbon::now()->format('Y-m-d H:i:s'); 
            $qu->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
