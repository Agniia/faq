<?php

namespace App\Http\Controllers;
use App\Category;
use App\Question;
use App\Answer;
use App\Admin;
use Carbon\Carbon;

use Illuminate\Http\Request;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\QuestionController;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $links = array('Список администраторов' => '/admin/list',
                'Создать нового администра' => '/admin/create',
                'Просмoтреть список тем' => '/admin/categories',
                'Создать новую тему' => '/admin/category/create',
                'Просмoтреть все вопросы без ответа' => '/admin/questions/noanswer'
        );

    public function index(Request $request)
    {
    	$title = ($login = $request->session()->get('login')) ? 'Вы авторизованы как '.$login: 'Войти';
        $view = view('pages.login')->with('title', $title); 
    	return view('pages.admin')->with('links', $this->links)->with('cont', $view); 
    }
                      
     public function createCategory(Request $request)
     {
       if( !$request->session()->get('login') )  
            return redirect()->route('home');
        $cat = new CategoryController;
        $cont = $cat->create();
    	return view('pages.admin')->with('links', $this->links)->with('cont', $cont); 
    }
                    
    public function getCatLinkQuList(Request $request)
    {
        if( !$request->session()->get('login') )  
            return redirect()->route('home');        
            
        $cat = new CategoryController;
        $cont = $cat->index();
    	return view('pages.admin')->with('links', $this->links)->with('cont', $cont); 
    }
    
    public function showCategory(Request $request)
    {
       if( !$request->session()->get('login') )  
            return redirect()->route('home');   
            
       $arr = explode('/',$request->path());
       $cat = new CategoryController;
       $cont = $cat->show($arr[count($arr)-1], 1);
       return view('pages.admin')->with('links', $this->links)->with('cont', $cont); 
    }
        
    public function destroyCategory(Request $request)
    {
       if( !$request->session()->get('login') )  
            return redirect()->route('home');   
            
       $arr = explode('/',$request->path());
       $cat = new CategoryController;
       $cat->destroy($arr[count($arr)-1]);      
       $ques = Question::where('category_id','=', $arr[count($arr)-1])->delete();
       return redirect()->route('cats_list');   
    }
              
    public function showNoAnswerQuestion(Request $request)
    {
        if( !$request->session()->get('login') )  
            return redirect()->route('home');   
            
        $cont = Question::select()->where('status_id', '=', 1)->get();
        $view = view('pages.questions_noanswer')->with('cont', $cont);
        return view('pages.admin')->with('links', $this->links)->with('cont', $view); 
     } 
    
     public function showQuestions(Request $request)
     {
        if( !$request->session()->get('login') )  
            return redirect()->route('home');   
            
        $arr = explode('/',$request->path());
        $cat = Category::find($arr[count($arr)-1]);
        $qu = new QuestionController;
        $cont = $qu->showQuestionsByCategory($arr[count($arr)-1],1);
    	return view('pages.admin')->with('links', $this->links)->with('cont', $cont);  
    }
        
     public function editCategory(Request $request)
     {
        if( !$request->session()->get('login') )  
            return redirect()->route('home');   
            
        $arr = explode('/',$request->path());
        $cat = new CategoryController;
        $cont = $cat->edit($arr[count($arr)-1]);
    	return view('pages.admin')->with('links', $this->links)->with('cont', $cont);  
    }
            
     public function updateCategory(Request $request)
     {
        if( !$request->session()->get('login') )  
            return redirect()->route('home');   
        
        $arr = explode('/',$request->path());
        $cat = new CategoryController;
        $cont = $cat->update( $request, $arr[count($arr)-1]);
    	return view('pages.admin')->with('links', $this->links)->with('cont', $cont);  
    }
    
     public function storeCategory(Request $request)
     {
        if( !$request->session()->get('login') )  
            return redirect()->route('home');   
        
        $arr = explode('/',$request->path());
        $cat = new CategoryController;
        $cont = $cat->store($request);
    	return view('pages.admin')->with('links', $this->links)->with('cont', $cont);  
    }
    
    public function showQuestion($id, Request $request)
    {
       if( !$request->session()->get('login') )  
            return redirect()->route('home');   
            
        $qu = new QuestionController;
        $cont = $qu->show($id);
        return view('pages.admin')->with('links', $this->links)->with('cont', $cont); 
    }  
    
    public function editQuestion(Request $request)
    {
        if( !$request->session()->get('login') )  
            return redirect()->route('home');   
            
        $arr = explode('/',$request->path());
        $qu = new QuestionController;
        $cont = $qu->edit($arr[count($arr)-1]);
    	return view('pages.admin')->with('links', $this->links)->with('cont', $cont);  
    }
    
    public function updateQuestion(Request $request)
    {
        if( !$request->session()->get('login') )  
            return redirect()->route('home');   
            
        if($request->answer_id == NULL)
        {
            $answer = new Answer;
            $answer->question_id = $request->question;
            $answer->text = $request->answer;
            $answer->created_at = Carbon::now()->format('Y-m-d H:i:s');
            $answer->updated_at = Carbon::now()->format('Y-m-d H:i:s'); 
            $answer->save();
        }
        else
        {
            $answer = Answer::find($request->answer_id);
            $answer->question_id = $request->question;
            $answer->text = $request->answer;
            $answer->updated_at = Carbon::now()->format('Y-m-d H:i:s'); 
            $answer->save();
        }   
        $qu = new QuestionController;
        $qu->update($request, $request->question);
        $cont = $qu->edit($request->question);
    	return view('pages.admin')->with('links', $this->links)->with('cont', $cont); 
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        if( !$request->session()->get('login') )  
            return redirect()->route('home');   
            
        $title = 'Добавить администратора';
        $view = view('pages.create_admin')->with('title', $title); 
    	return view('pages.admin')->with('links', $this->links)->with('cont', $view); 
    }
    
    public function login(Request $request)
    {
        $admin = Admin::select()->where('login', '=', $request->login)->get()->toArray();
        if(md5($request->password) == $admin[0]['password'])
        {
            $request->session()->forget('login');
            $request->session()->put('login', $admin[0]['login']);
        }
        $value = $request->session()->get('login');
        
        $title = 'Вы авторизованы как '.$value ;
        $view = view('pages.login')->with('title', $title); 
    	return view('pages.admin')->with('links', $this->links)->with('cont', $view); 
    }
    
    public function logout(Request $request)
    {
          $request->session()->forget('login');
          return redirect()->route('home');
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if( !$request->session()->get('login') )  
            return redirect()->route('home');   
            $title = '';
            $admin = Admin::select()->where('login', '=', $request->login)->first();
            if($admin){
                $title = 'Администратор с логином "'.$request->login.'" уже существует';
    	    }
    	    else{
    	        $admin = new Admin;
    	        $admin->login = $request->login;
                $admin->password = md5($request->password);
                $admin->created_at = Carbon::now()->format('Y-m-d H:i:s');
                $admin->updated_at = Carbon::now()->format('Y-m-d H:i:s'); 
                $title = ($admin->save()) ? 'Администратор "'.$request->login.'" создан . Добавить администратора' : 'Что-то пошло не так';
     	    }
            $view = view('pages.create_admin')->with('title', $title); 
            return view('pages.admin')->with('links', $this->links)->with('cont', $view);  
    }
    
    public function showAll(Request $request)
    {
        if( !$request->session()->get('login') )  
            return redirect()->route('home');   
        $admins = Admin::all()->toArray();
        $title = 'Список администраторов';
        $view = view('pages.admins_list')->with('title', $title)->with('admins', $admins); 
        return view('pages.admin')->with('links', $this->links)->with('cont', $view);  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id, Request $request)
    {
        if( !$request->session()->get('login') )  
            return redirect()->route('home');   
            
        $admin = Admin::find($id)->toArray();
        $title = 'Aдминистратор';
        $view = view('pages.admin_info')->with('title', $title)->with('admin', $admin); 
        return view('pages.admin')->with('links', $this->links)->with('cont', $view);  
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        if( !$request->session()->get('login') )  
            return redirect()->route('home');   
            
        $admin = Admin::find($id)->toArray();
        $title = 'Изменить пароль администратора';
        $view = view('pages.edit_admin')->with('title', $title)->with('admin', $admin); 
        return view('pages.admin')->with('links', $this->links)->with('cont', $view);  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if( !$request->session()->get('login') )  
            return redirect()->route('home');   
        
        $admin = Admin::find($id);
        $admin->password = md5($request->password);
            return redirect()->route('show_admin', ['admin_id' => $id]);   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        if( !$request->session()->get('login') )  
            return redirect()->route('home');  
         
         $admin = Admin::find($id);
         $admin->delete();   
         
         return redirect()->route('show_all');   
    }
}
