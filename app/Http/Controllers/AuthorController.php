<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Author;
use App\Http\Controllers\QuestionController;
use App\Category;
use App\Question;
use Carbon\Carbon;

class AuthorController extends Controller
{
     protected $links;
     
     public function __construct()
     {
             $cats = Category::all('id','name')->toArray();
             $this->links = [];
             $this->links['Задать вопрос'] =  '/faq/question/create';
             foreach($cats as $cat){
                $name = $cat['name'];
                $id = $cat['id'];
                $this->links["$name"] = '/faq/category/show/'.$id;
            }
    }
        
    public function showCategory(Request $request)
    {
       $arr = explode('/',$request->path());
       $qu = new QuestionController;
       $quList = $qu->showQuestionsByCategory($arr[count($arr)-1], 0);
       return view('pages.faq')->with('links', $this->links)->with('quList', $quList); 
    }
    
    public function createQuestion()
    {
        $qu = new QuestionController;
        $quForm = $qu->create();
        return view('pages.faq')->with('links', $this->links)->with('quForm', $quForm); 
    }
    
    public function storeQuestion(Request $request)
    {
        $author = Author::where('email', '=', $request->email)->first();
        $qu = new QuestionController;
        $cont = '';
       if($author)
              $cont = $qu->store($request, $author->id);
        else{
                $newAuthor = $this->store($request);
                if($newAuthor)
                    $cont = $qu->store($request, $newAuthor->id);
        }
        return view('pages.faq')->with('links', $this->links)->with('cont', $cont); 
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Часто задаваемые вопросы';
        // get list of questions with answers
    	return view('pages.faq')->with('links', $this->links)->with('title', $title);     
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $newAuthor = new Author;
            $newAuthor->name = $request->author;
            $newAuthor->email = $request->email;
            $newAuthor->created_at = Carbon::now()->format('Y-m-d H:i:s');
            $newAuthor->updated_at = Carbon::now()->format('Y-m-d H:i:s'); 
            $newAuthor->save();
            return $newAuthor;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function show(Author $author)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function edit(Author $author)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Author $author)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy(Author $author)
    {
        //
    }
}
