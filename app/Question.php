<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public function category()
    {
        return $this->hasOne('App\Category');
    }
    
	 public function questionsLinkCatTotalInfo($category_id)
	 {
		return \DB::select("SELECT questions.*, authors.name as author_name, statuses.name as status_name FROM questions JOIN authors ON author_id = authors.id JOIN statuses ON status_id = statuses.id WHERE category_id = $category_id");
	}
	    
	 public function questionsLinkCatTotalInfoPublished($category_id)
	 {
		return \DB::select("SELECT questions.*, authors.name as author_name, answers.text as answer FROM questions JOIN authors ON author_id = authors.id JOIN answers ON questions.id = answers.question_id WHERE category_id = $category_id AND status_id = 2");
	}	    
	    
	 public function questionWithAnswerTotalInfo($question_id)
	 {
		return \DB::select("SELECT questions.*, authors.name as author_name, authors.id as author_id, categories.name as category_name, statuses.name as status_name, answers.id as  answer_id, answers.text as answer FROM questions JOIN authors ON author_id = authors.id JOIN statuses ON status_id = statuses.id 
		JOIN  categories ON  categories.id = questions.category_id
		LEFT JOIN answers ON answers.question_id = questions.id WHERE questions.id = $question_id");
	}
}
