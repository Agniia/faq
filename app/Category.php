<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{	 
     public function question()
    {
         return $this->hasMany('App\Question');
    }

	 public function catLinkQuestionCount()
	 {
		return \DB::table('categories')->select(\DB::raw('categories.id as category_id, categories.name as category_name, ( SELECT COUNT(questions.id) FROM questions WHERE questions.category_id = categories.id ) as questions_amount, ( SELECT COUNT(questions.id) FROM questions WHERE questions.category_id = categories.id AND questions.status_id = 1) as no_answer, ( SELECT COUNT(questions.id) FROM questions WHERE questions.category_id = categories.id AND questions.status_id = 2) as published'))
	    ->get();
	}
}
