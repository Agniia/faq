<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return View::make('pages.home');
})->name('home');

Route::get('/faq', 'AuthorController@index');

Route::get('/faq/category/show/{category}', 'AuthorController@showCategory');

Route::get('/faq/question/create', 'AuthorController@createQuestion');

Route::get('/faq/question/store', 'AuthorController@storeQuestion');

Route::get('/admin', 'AdminController@index');

Route::get('/admin/create', 'AdminController@create')->name('create_admin');

Route::post('/admin/store', 'AdminController@store');

Route::post('/admin/login', 'AdminController@login');

Route::get('/admin/logout', 'AdminController@logout');

Route::get('/admin/list', 'AdminController@showAll')->name('show_all');

Route::get('/admin/show/{admin_id}', 'AdminController@show')->name('show_admin');

Route::get('/admin/edit/{admin_id}', 'AdminController@edit');

Route::post('/admin/update/{admin_id}', 'AdminController@update');

Route::get('/admin/destroy/{admin_id}', 'AdminController@destroy');

Route::get('/admin/categories', 'AdminController@getCatLinkQuList')->name('cats_list');

Route::get('/admin/category/show/{category}', 'AdminController@showCategory');

Route::get('/admin/questions/category/{category_id}', 'AdminController@showQuestions');

Route::get('/admin/question/edit/{question_id}', 'AdminController@editQuestion');

Route::post('/admin/question/update/{question_id}', 'AdminController@updateQuestion');

Route::get('/admin/questions/noanswer', 'AdminController@showNoAnswerQuestion');

Route::get('/admin/category/edit/{category_id}', 'AdminController@editCategory');

Route::get('/admin/category/update/{category_id}', 'AdminController@updateCategory');

Route::get('/admin/category/destroy/{category_id}', 'AdminController@destroyCategory');

Route::get('/admin/category/create', 'AdminController@createCategory');

Route::get('/admin/category/store', 'AdminController@storeCategory');